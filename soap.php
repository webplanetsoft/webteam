<?php
/**
 * local Socap Client Controller clas file
 * @uses : 
 *
 * @params :
 **/

class LocalSoapClient extends SoapClient
{

  public function __call($function_name, $arguments)
  {
    $result = false;
    $max_retries = 5;
    $retry_count = 0;
   
    while(! $result && $retry_count < $max_retries)
    {
      try
      {
        $result = parent::__call($function_name, $arguments);
      }
      catch(SoapFault $fault)
      {
        if($fault->faultstring != 'Could not connect to host')
        {
          throw $fault;
        }
      }
      sleep(1);
      $retry_count ++;
    }
    if($retry_count == $max_retries)
    {
      throw new SoapFault('Could not connect to host after 5 attempts');
    }
    return $result;
  }
}


/*************Use of Soap Class***********************/

$wsdl = '';
echo file_get_contents($wsdl);
die('http://192.234.10.104:8080/bvnV/bvnV?wsdl');


echo "<pre>";
$client = new SoapClient("http://192.234.10.104:8080/bvnV/bvnV?wsdl");
$client->__soapCall("verifySingleBVN", array('2222222','321654'));


print_r($client);die;
var_dump($client->checkVat(array(
  'countryCode' => $countryCode,
  'vatNumber' => $vatNo
)));
?>
